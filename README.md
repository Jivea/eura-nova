# README #


### What? ###

Implementation of Eura-Nova Node.js/React case.

### Getting started ###

#### To run the application on your computer: #### 
* Install Node.js
* Get the sources
* Run src/updateDependencies.bat
* Run command "node src/euranova"

#### Developers: ####
This project uses Grunt as automation tool.

Here is the list of available commands:

* Run automated tests: "grunt test"
* Run javascript syntax verification: "grunt lint"

### Contributors ###
* Code by J�r�me Vanhoof