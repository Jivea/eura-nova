/**
 * Created by Jérôme Vanhoof.
 * Tests the server.js class
 */

"use strict";
var PORT = 8083;
var GENERATED_FOLDER = "artifacts/tests";
var GENERATED_HOMEPAGE = "pageHome.html";
var GENERATED_404PAGE = "page404.html";

var server = require("./server.js");
var http = require("http");
var fs = require("fs");
var assert = require("assert");


exports.tearDown = function (done) {

    cleanUp(GENERATED_FOLDER + "/" + GENERATED_HOMEPAGE);
    cleanUp(GENERATED_FOLDER + "/" + GENERATED_404PAGE);

    if (server.isStarted()) {
        server.stop(function () {
            done();
        });
    } else {
        done();
    }
};

exports.test_servesHomePageWhenRootUrl = function (test) {
    var fileContent = "Hello World";
    fs.writeFileSync(GENERATED_FOLDER + '/' + GENERATED_HOMEPAGE, fileContent);
    httpGet("http://localhost:" + PORT, function (response, responseData) {
        test.equals(200, response.statusCode, "Returned status code is not correct");
        test.equals(fileContent, responseData, "response content not OK");
        test.ok(responseData, "Should have received data");
        test.done();
    });
};

exports.test_serves404PageWhenRandomUrl = function (test) {
    var fileContent = "This is the 404";
    fs.writeFileSync(GENERATED_FOLDER + '/' + GENERATED_404PAGE, fileContent);

    httpGet("http://localhost:" + PORT + "/random", function (response, responseData) {
        test.equals(404, response.statusCode, "Returned status code is not correct");
        test.equals(fileContent, responseData, "response content not OK");
        test.done();
    });
};

exports.test_servesSpecificPage = function (test) {
    var fileContent = "Hello World";
    fs.writeFileSync(GENERATED_FOLDER + '/' + GENERATED_HOMEPAGE, fileContent);

    httpGet("http://localhost:" + PORT + "/" + GENERATED_HOMEPAGE, function (response, responseData) {
        test.equals(200, response.statusCode, "Returned status code is not correct");
        test.equals(fileContent, responseData, "response content not OK");
        test.done();
    });
};

exports.test_stopCallsTheCallback = function (test) {
    server.start(GENERATED_FOLDER, GENERATED_HOMEPAGE, GENERATED_404PAGE, PORT, function () {
        server.stop(function () {
            test.done();
        });
    });
};

exports.test_isStartedFollowsServerState = function (test) {
    test.ok(!server.isStarted());

    server.start(GENERATED_FOLDER, GENERATED_HOMEPAGE, GENERATED_404PAGE, PORT, function () {
        test.ok(server.isStarted(), "isStarted should return true");

        server.stop(function () {
            test.ok(!server.isStarted(), "isStarted should return false");
            test.done();
        });
    });
};

function httpGet(url, callback) {
    server.start(GENERATED_FOLDER, GENERATED_HOMEPAGE, GENERATED_404PAGE, PORT, function () {
        http.get(url, function (response) {
            var receivedData = "";
            response.setEncoding("utf8");

            response.on("data", function (chunk) {
                receivedData = chunk;
            });

            response.on("end", function () {
                callback(response, receivedData);
            });
        });
    });
}

function cleanUp(file) {
    if (fs.existsSync(GENERATED_HOMEPAGE)) {
        fs.unlinkSync(GENERATED_HOMEPAGE);
        assert.ok(!fs.existsSync(GENERATED_HOMEPAGE), "Could not delete file:" + GENERATED_HOMEPAGE);
    }
}