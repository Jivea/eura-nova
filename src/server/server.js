/**
 * Created by Jérôme Vanhoof
 * Server
 * + Serves webpages and files in the public folder
 * + Routes api calls to appropriate controllers
 */

'use strict';

var express = require('express');
var app = express();

var _server ;
var _isStarted ;

exports.start = function(publicFolder, homePage, notFoundPage, portNumber, callback) {

    _server = app.listen(portNumber, function () {
        _isStarted=true;
        callback();
    });

    app.use(express.static(publicFolder));

    var options = {
        root: publicFolder +'/',
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    app.get('/', function (req, res) {
        res.sendFile(homePage, options, function(err) {
            if(err) {
                console.log(err);
                res.status(err.status).end();
            }
        });
    });

    // The 404 Route (ALWAYS Keep this as the last route)
    app.get('*', function (req, res) {
        res.status(404).sendFile(notFoundPage, options, function (err) {
            if (err) {
                console.log(err);
                res.status(err.status).end();
            }
        });
    });
};

exports.stop = function(callback) {
    _server.close(function(){
        _isStarted = false;
        callback();
    });
};

exports.isStarted = function() {
    return _isStarted;
};