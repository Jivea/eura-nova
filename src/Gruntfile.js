module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        nodeunit: {
            all: [  "server/*_test.js"],
            options: {
                reporter: 'tap'
            }
        },
        jshint: {
            node: {
                src: nodeFiles(),
                options: nodeLintOptions()
            }
        }
    });

    // Load grunt plugins
    grunt.loadNpmTasks("grunt-contrib-nodeunit");
    grunt.loadNpmTasks("grunt-contrib-jshint");

    // Register tasks
    grunt.registerTask("default", "Test", ["test"]);
    grunt.registerTask("lint", "Lint", ["jshint"]);
    grunt.registerTask("test", "Test everything", ["nodeunit"]);

    function globalLintOptions() {
        return {
            bitwise: true,
            curly: false,
            eqeqeq: true,
            forin: true,
            immed: true,
            latedef: false,
            newcap: true,
            noarg: true,
            noempty: true,
            nonew: true,
            regexp: true,
            undef: true,
            strict: true,
            trailing: true
        };
    }
    function nodeLintOptions() {
        var options = globalLintOptions();
        options.node = true;
        return options;
    }

    function nodeFiles() {
        return [    "Gruntfiles.js",
            "euranova.js",
            "server/**/*.js"];
    }
};