/**
 * Created by Jivea
 * Launch the Guga server.
 */
(function(){
    "use strict";

    var server = require("./server/server.js");
    var port =  process.env.PORT || 1337;
    server.start("./public","index.html", "404.html", port, function(){
        console.log("Server started");
        console.log("Browse to http://localhost:" + port);
    });
})();